package jamshid.task_apelsinserver.controller;

import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/category")
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/list/{page}")
    public HttpEntity<?> getAllCategories(@PathVariable Integer page) {
        ApiResponse apiResponse = categoryService.getAllCategories(page);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/detail/")
    public HttpEntity<?> getCategoryByProductId(@RequestParam Integer product_id) {
        ApiResponse apiResponse = categoryService.getCategoryByProductId(product_id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


}
