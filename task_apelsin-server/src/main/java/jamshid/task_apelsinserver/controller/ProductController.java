package jamshid.task_apelsinserver.controller;

import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/list/{page}")
    public HttpEntity<?> getProductList(@PathVariable Integer page){
        ApiResponse productList = productService.getProductList(page);
        return ResponseEntity.status(productList.isSuccess()?200:409).body(productList);
    }

    @GetMapping("/detail/")
    public HttpEntity<?> getProductById(@RequestParam Integer product_id){
        ApiResponse productList = productService.getProductById(product_id);
        return ResponseEntity.status(productList.isSuccess()?200:409).body(productList);
    }



}
