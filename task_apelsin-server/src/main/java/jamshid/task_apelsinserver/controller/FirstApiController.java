package jamshid.task_apelsinserver.controller;

import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.service.FirstApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("firstapi/")
public class FirstApiController {
    private final FirstApiService firstApiService;

    @Autowired
    public FirstApiController(FirstApiService firstApiService) {
        this.firstApiService = firstApiService;
    }

//    1-api
    @GetMapping("/expired_invoices")
    public HttpEntity<?> getExpiredInvoices() {
        ApiResponse apiResponse = firstApiService.getExpiredInvoices();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

//    2-api
    @GetMapping("/wrong_date_invoices")
    public HttpEntity<?> getWrongDateInvoices() {
        ApiResponse apiResponse = firstApiService.getWrongDateInvoices();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

//    3-api
    @GetMapping("/orders_without_details")
    public HttpEntity<?> getOrdersWithoutDetails() {
        ApiResponse apiResponse = firstApiService.getOrdersWithoutDetails();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

//    4-api
    @GetMapping("/customers_without_orders")
    public HttpEntity<?> getCustomersWithoutOrders() {
        ApiResponse apiResponse = firstApiService.getCustomersWithoutOrders();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

//    5-api
    @GetMapping("/customers_last_orders")
    public HttpEntity<?> getCustomersLastOrders() {
        ApiResponse apiResponse = firstApiService.getCustomersLastOrders();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //    6-api
    @GetMapping("/overpaid_invoices")
    public HttpEntity<?> getOverpaidInvoices() {
        ApiResponse apiResponse = firstApiService.getOverpaidInvoices();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //    7-api
    @GetMapping("/high_demand_products")
    public HttpEntity<?> getHighDemandProducts() {
        ApiResponse apiResponse = firstApiService.getHighDemandProducts();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //    8-api
    @GetMapping("/bulk_products")
    public HttpEntity<?> getBulkProducts() {
        ApiResponse apiResponse = firstApiService.getBulkProducts();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

//        9-api
    @GetMapping("/number_of_products_in_year")
    public HttpEntity<?> getNumberOfProductsInYear() {
        ApiResponse apiResponse = firstApiService.getNumberOfProductsInYear();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //        10-api
    @GetMapping("/orders_without_invoices")
    public HttpEntity<?> getOrdersWithoutInvoices() {
        ApiResponse apiResponse = firstApiService.getOrdersWithoutInvoices();
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
