package jamshid.task_apelsinserver.controller;

import jamshid.task_apelsinserver.payload.OrderDto;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.service.OrderService;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/add")
    public HttpEntity<?> makeOrder(@RequestBody OrderDto dto){
        ApiResponse apiResponse = orderService.makeOrder(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/details")
    public HttpEntity<?> getOrderById(@RequestParam Integer order_id){
        ApiResponse apiResponse = orderService.getOrderById(order_id);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
}
