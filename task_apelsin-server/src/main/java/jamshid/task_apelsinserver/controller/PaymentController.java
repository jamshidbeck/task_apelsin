package jamshid.task_apelsinserver.controller;

import jamshid.task_apelsinserver.payload.PaymentDto;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/payment")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }


    @PostMapping("/")
    public HttpEntity<?> makePayment(@RequestBody PaymentDto dto){
        ApiResponse apiResponse = paymentService.makePayment(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/details")
    public HttpEntity<?> getPaymentById(@RequestParam Integer payment_id){
        ApiResponse apiResponse = paymentService.getPaymentById(payment_id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
