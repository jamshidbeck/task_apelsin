package jamshid.task_apelsinserver.entity;

import jamshid.task_apelsinserver.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Detail extends BaseEntity {
    @ManyToOne
    private Order order;

    @ManyToOne
    private Product product;

    @Column(columnDefinition = "smallint")
    private int quantity;

}
