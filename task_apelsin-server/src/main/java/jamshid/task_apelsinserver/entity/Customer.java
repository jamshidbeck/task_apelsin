package jamshid.task_apelsinserver.entity;

import jamshid.task_apelsinserver.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Customer extends BaseEntity {

    @Column(nullable = false, columnDefinition = "VARCHAR(14)")
    private String name;

    @Column(nullable = false, columnDefinition = "CHAR(3)")
    private String country;

    @Column(columnDefinition = "TEXT")
    private String address;

    @Column(columnDefinition = "VARCHAR(50)")
    private String phone;

}
