package jamshid.task_apelsinserver.entity;

import jamshid.task_apelsinserver.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends BaseEntity {
    @Column(columnDefinition = "varchar(10)")
    private String name;

    @ManyToOne
    private Category category;

    @Column(columnDefinition = "varchar(20)")
    private String description;

    @Column(columnDefinition = "numeric(6,2)")
    private double price;

    @Column(columnDefinition = "varchar(1024)")
    private String photo;


}
