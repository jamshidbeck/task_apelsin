package jamshid.task_apelsinserver.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jamshid.task_apelsinserver.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Invoice extends BaseEntity {
    @OneToOne
    private Order order;
    @Column(nullable = false, columnDefinition = "numeric(8,2)")
    private float amount;

    @Column(nullable = false)
    private Date issued;

    @Column(nullable = false)
    private Date due;

}
