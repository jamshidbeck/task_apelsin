package jamshid.task_apelsinserver.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(columnDefinition = "timestamp default now()")
    private Date createdDate;

    @JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
    @LastModifiedDate
    private Date updatedDate;

    @Column(columnDefinition = "boolean default true")
    private boolean isActive;
}
