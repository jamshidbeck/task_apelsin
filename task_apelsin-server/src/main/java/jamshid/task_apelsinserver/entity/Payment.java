package jamshid.task_apelsinserver.entity;

import jamshid.task_apelsinserver.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Time;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Payment extends BaseEntity {
    @Column(nullable = false)
    private Time time;

    @Column(nullable = false, columnDefinition = "numeric(8,2)")
    private float amount;

    @ManyToOne
    private Invoice invoice;

}
