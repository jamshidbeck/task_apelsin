package jamshid.task_apelsinserver.service;

import jamshid.task_apelsinserver.entity.Invoice;
import jamshid.task_apelsinserver.entity.Payment;
import jamshid.task_apelsinserver.payload.PaymentDto;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.repository.InvoiceRepository;
import jamshid.task_apelsinserver.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    private final InvoiceRepository invoiceRepository;
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(InvoiceRepository invoiceRepository, PaymentRepository paymentRepository) {
        this.invoiceRepository = invoiceRepository;
        this.paymentRepository = paymentRepository;
    }

    public ApiResponse makePayment(PaymentDto dto){
        try {
            Invoice invoice = invoiceRepository.findById(dto.getInvoice_id()).orElseThrow(() -> new IllegalStateException("Invoice not found"));
            Payment payment = new Payment();
            payment.setAmount(invoice.getAmount());
            payment.setInvoice(invoice);
            payment.setTime(new java.sql.Time(System.currentTimeMillis()));
            paymentRepository.save(payment);
            return new ApiResponse(true, "SUCCESS");
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }

    public ApiResponse getPaymentById(Integer payment_id) {
        try{
            Payment payment = paymentRepository.findById(payment_id).orElseThrow(() -> new IllegalStateException("Payment not found"));
            return new ApiResponse(true, "SUCCESS", payment);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }
}
