package jamshid.task_apelsinserver.service;

import jamshid.task_apelsinserver.entity.Product;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class ProductService {
    private final JdbcTemplate jdbcTemplate;
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(JdbcTemplate jdbcTemplate, ProductRepository productRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.productRepository = productRepository;
    }

    public ApiResponse getProductList(Integer page) {
        try {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from product limit 20 offset ?", page);
            return new ApiResponse(true, "SUCCESS", maps);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }

    public ApiResponse getProductById(Integer id) {
        try {
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select p.id, p.name, p.price, p.category_id, p.description, p.photo from product p left join category c on c.id = p.category_id where p.id=?", id);
            return new ApiResponse(true, "SUCCESS", maps);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }
}
