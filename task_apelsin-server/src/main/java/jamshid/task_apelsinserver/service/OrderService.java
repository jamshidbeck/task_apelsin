package jamshid.task_apelsinserver.service;

import jamshid.task_apelsinserver.entity.*;
import jamshid.task_apelsinserver.payload.OrderDto;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final InvoiceRepository invoiceRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final DetailRepository detailRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, InvoiceRepository invoiceRepository, CustomerRepository customerRepository, ProductRepository productRepository, DetailRepository detailRepository) {
        this.orderRepository = orderRepository;
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.detailRepository = detailRepository;
    }

    public ApiResponse makeOrder(OrderDto dto){
        Order order = new Order();
        try{
            Customer customer = customerRepository.findById(dto.getCustomer_id()).orElseThrow(() -> new IllegalStateException("Customer not found"));
            Product product = productRepository.findById(dto.getProduct_id()).orElseThrow(() -> new IllegalStateException("Product not found"));
            order.setCustomer(customer);
            order.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
            order.setActive(true);
            Order savedOrder = orderRepository.save(order);

            Detail detail = new Detail();
            detail.setOrder(savedOrder);
            detail.setProduct(product);
            detail.setQuantity(dto.getQuantity());
            detail.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
            detail.setActive(true);
            detailRepository.save(detail);

            Invoice invoice = new Invoice();
            invoice.setOrder(savedOrder);
            invoice.setAmount((float) (dto.getQuantity() * product.getPrice()));
            invoice.setIssued(new java.sql.Date(System.currentTimeMillis()));
            invoice.setDue(new java.sql.Date(System.currentTimeMillis()));
            invoice.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
            invoice.setActive(true);
            invoiceRepository.save(invoice);
            return new ApiResponse(true, "SUCCESS");
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }

    public ApiResponse getOrderById(Integer order_id){
        try{
            Order order = orderRepository.findById(order_id).orElseThrow(() -> new IllegalStateException("Order not found"));
            return new ApiResponse(true, "SUCCESS", order);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }

    }
}
