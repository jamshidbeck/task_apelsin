package jamshid.task_apelsinserver.service;

import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FirstApiService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public FirstApiService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //1
    public ApiResponse getExpiredInvoices() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select i.due, i.issued, i.order_id, i.amount\n" +
                    "from invoice i left join orders o on i.order_id = o.id\n" +
                    "where i.issued > i.due");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }

    //2
    public ApiResponse getWrongDateInvoices() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select i.id, i.issued, i.order_id, o.created_date order_date from invoice i left join orders o on i.order_id = o.id where o.created_date > i.issued");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }

    //3
    public ApiResponse getOrdersWithoutDetails() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select o.id, o.created_date, o.is_active, o.updated_date, o.customer_id from orders o left join detail d on o.id = d.order_id where d.order_id is null and o.created_date < '2016-09-06'");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }


    //4
    public ApiResponse getCustomersWithoutOrders() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id, c.name, c.country, c.address, c.phone from customer c left join orders o on c.id = o.customer_id where extract(year from o.created_date) != 2016");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }

    //5
    public ApiResponse getCustomersLastOrders() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.id, c.name, o.created_date last_order_date\n" +
                    "from customer c\n" +
                    "         left join orders o\n" +
                    "                    on c.id = o.customer_id\n" +
                    "                        where o.id = (\n" +
                    "                            SELECT subOrders.id\n" +
                    "                            FROM orders subOrders\n" +
                    "                            WHERE subOrders.customer_id = o.customer_id\n" +
                    "                            ORDER BY subOrders.created_date desc limit 1\n" +
                    "                        )");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }


//    6
    public ApiResponse getOverpaidInvoices() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select i.id, (select sum(amount) from payment where invoice_id = i.id)-i.amount reimbursed from invoice i where i.amount < (select sum(amount) from payment where invoice_id = i.id)\n");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }

    //    7
    public ApiResponse getHighDemandProducts() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select p.id, (select count(d.product_id) from detail d where d.product_id = p.id) total_number from product p where (select count(d.product_id) from detail d where d.product_id = p.id) >= 10");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }


    //    8
    public ApiResponse getBulkProducts() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select distinct p.id, p.price from product p left join detail d on p.id = d.product_id where d.quantity>=8");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }


//    9
    public ApiResponse getNumberOfProductsInYear() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select c.country, count(o.customer_id) order_count from customer c left join orders o on c.id = o.customer_id\n" +
                    "where extract(year from o.created_date) = 2016 group by c.country");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }

    //    10
    public ApiResponse getOrdersWithoutInvoices() {
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select b.id product_id, b.created_date ordered_date, b.quantity, p.price, b.quantity*p.price total_price from (select a.id, a.created_date, d.quantity, d.product_id from (select o.id, o.created_date from orders o left join invoice i on o.id = i.order_id where i.order_id is null) a left join detail d on d.id=a.id) b left join product p on p.id=b.product_id");
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false,"ERROR");
        }
    }
}
