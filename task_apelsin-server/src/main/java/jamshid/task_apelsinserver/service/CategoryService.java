package jamshid.task_apelsinserver.service;

import jamshid.task_apelsinserver.entity.Product;
import jamshid.task_apelsinserver.payload.response.ApiResponse;
import jamshid.task_apelsinserver.repository.CategoryRepository;
import jamshid.task_apelsinserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CategoryService {
    private final JdbcTemplate jdbcTemplate;
    private final ProductRepository productRepository;

    @Autowired
    public CategoryService(JdbcTemplate jdbcTemplate, ProductRepository productRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.productRepository = productRepository;
    }

    public ApiResponse getAllCategories(Integer page){
        try{
            List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from category limit 20 offset ?", page);
            return new ApiResponse(true, "SUCCESS", maps);
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }

    public ApiResponse getCategoryByProductId(Integer productId){
        try{
            Optional<Product> byId = productRepository.findById(productId);
            return byId.map(product ->
                    new ApiResponse(true, "SUCCESS", product.getCategory())).orElseGet(() -> new ApiResponse(false, "Product not found"));
        }catch (Exception e){
            e.printStackTrace();
            return new ApiResponse(false, "NOT SUCCESS");
        }
    }
}
