package jamshid.task_apelsinserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Integer customer_id;
    private Integer product_id;
    private Integer quantity;
}
